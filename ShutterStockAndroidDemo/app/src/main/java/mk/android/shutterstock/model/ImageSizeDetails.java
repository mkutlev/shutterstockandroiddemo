package mk.android.shutterstock.model;

public class ImageSizeDetails {

    private int width;
    private int height;
    private int fileSize;
    private String displayName;
    private int dpi;
    private String format;
    private boolean isLicensable;

    public ImageSizeDetails() {
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public int getDpi() {
        return dpi;
    }

    public void setDpi(int dpi) {
        this.dpi = dpi;
    }

    public boolean isLicensable() {
        return isLicensable;
    }

    public void setLicensable(boolean isLicensable) {
        this.isLicensable = isLicensable;
    }

}
