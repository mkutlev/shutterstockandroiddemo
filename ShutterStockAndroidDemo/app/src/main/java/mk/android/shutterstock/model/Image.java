package mk.android.shutterstock.model;

public class Image {

    private String id;
    private String description;
    private String addedDate;
    private String mediaType;

    private Category[] categories;

    private ImageAssets assets;

    public Image() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public Category[] getCategories() {
        return categories;
    }

    public void setCategories(Category[] categories) {
        this.categories = categories;
    }

    public ImageAssets getAssets() {
        return assets;
    }

    public void setAssets(ImageAssets imageAssets) {
        this.assets = imageAssets;
    }

}
