package mk.android.shutterstock.model;

public class ImageAssets {

    private ImageSizeDetails smallJpg;
    private ImageSizeDetails mediumJpg;
    private ImageSizeDetails hugeJpg;
    private ImageSizeDetails supersizeJpg;
    private ImageSizeDetails hugeTiff;
    private ImageSizeDetails supersizeTiff;
    private ImageSizeDetails vectorEps;

    private Thumbnail smallThumb;
    private Thumbnail largeThumb;
    private Thumbnail preview;
    private Thumbnail preview1000;

    public ImageAssets() {

    }

    public ImageSizeDetails getSmallJpg() {
        return smallJpg;
    }

    public void setSmallJpg(ImageSizeDetails smallJpg) {
        this.smallJpg = smallJpg;
    }

    public ImageSizeDetails getMediumJpg() {
        return mediumJpg;
    }

    public void setMediumJpg(ImageSizeDetails mediumJpg) {
        this.mediumJpg = mediumJpg;
    }

    public ImageSizeDetails getHugeJpg() {
        return hugeJpg;
    }

    public void setHugeJpg(ImageSizeDetails hugeJpg) {
        this.hugeJpg = hugeJpg;
    }

    public ImageSizeDetails getSupersizeJpg() {
        return supersizeJpg;
    }

    public void setSupersizeJpg(ImageSizeDetails supersizeJpg) {
        this.supersizeJpg = supersizeJpg;
    }

    public ImageSizeDetails getHugeTiff() {
        return hugeTiff;
    }

    public void setHugeTiff(ImageSizeDetails hugeTiff) {
        this.hugeTiff = hugeTiff;
    }

    public ImageSizeDetails getSupersizeTiff() {
        return supersizeTiff;
    }

    public void setSupersizeTiff(ImageSizeDetails supersizeTiff) {
        this.supersizeTiff = supersizeTiff;
    }

    public ImageSizeDetails getVectorEps() {
        return vectorEps;
    }

    public void setVectorEps(ImageSizeDetails vectorEps) {
        this.vectorEps = vectorEps;
    }

    public Thumbnail getSmallThumb() {
        return smallThumb;
    }

    public void setSmallThumb(Thumbnail smallThumb) {
        this.smallThumb = smallThumb;
    }

    public Thumbnail getLargeThumb() {
        return largeThumb;
    }

    public void setLargeThumb(Thumbnail largeThumb) {
        this.largeThumb = largeThumb;
    }

    public Thumbnail getPreview() {
        return preview;
    }

    public void setPreview(Thumbnail preview) {
        this.preview = preview;
    }

    public Thumbnail getPreview1000() {
        return preview1000;
    }

    public void setPreview1000(Thumbnail preview1000) {
        this.preview1000 = preview1000;
    }

}
