package mk.android.shutterstock.model;

public class ImagesResponse {

    private Image[] data;
    private int page;
    private int perPage;
    private int totalCount;
    private String searchId;

    public ImagesResponse() {
    }

    public Image[] getData() {
        return data;
    }

    public void setData(Image[] data) {
        this.data = data;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

}
