package mk.android.shutterstock.communication;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class NetworkCommunicationManager {

    private static final int MAX_CACHE_SIZE = 25;

    private static NetworkCommunicationManager instance;

    private ConnectivityManager connectivityManager;

    private RequestQueue requestQueue;
    private ImageLoader imageLoader;
    private static Context context;

    public static NetworkCommunicationManager getInstance(Context context) {
        if (instance == null) {
            instance = new NetworkCommunicationManager(context);
        }

        return instance;
    }

    public NetworkCommunicationManager(Context context) {
        NetworkCommunicationManager.context = context.getApplicationContext();
        requestQueue = getRequestQueue();

        imageLoader = new ImageLoader(requestQueue,
                new ImageLoader.ImageCache() {

                    private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(MAX_CACHE_SIZE);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });

        connectivityManager = (ConnectivityManager) NetworkCommunicationManager.context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    public void cancelAllRequests(Object tag) {
        getRequestQueue().cancelAll(tag);
    }

    public boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
