package mk.android.shutterstock.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import mk.android.shutterstock.R;
import mk.android.shutterstock.communication.NetworkCommunicationManager;
import mk.android.shutterstock.model.Image;

public class ImagesListAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ImageLoader imageLoader;

    private Image[] images;

    public ImagesListAdapter(Context context) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = NetworkCommunicationManager.getInstance(context).getImageLoader();

        images = new Image[0];
    }

    /**
     * Sets the images to be displayed and updates the list.
     *
     * @param images the new images
     */
    public void setImages(Image[] images) {
        this.images = images;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return images[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;

        if (itemView == null) {
            itemView = layoutInflater.inflate(R.layout.adapter_images_list_item, parent, false);
        }

        Image image = images[position];

        NetworkImageView imageView = (NetworkImageView) itemView.findViewById(R.id.images_adapter_image);
        // Set the URL of the image that should be loaded into this view, and
        // specify the ImageLoader that will be used to make the request.
        imageView.setImageUrl(image.getAssets().getPreview().getUrl(), imageLoader);

        TextView descriptionView = (TextView) itemView.findViewById(R.id.images_adapter_description);
        descriptionView.setText(image.getDescription());

        return itemView;
    }

}
