package mk.android.shutterstock.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import mk.android.shutterstock.R;
import mk.android.shutterstock.communication.GsonRequest;
import mk.android.shutterstock.communication.NetworkCommunicationManager;
import mk.android.shutterstock.model.Image;
import mk.android.shutterstock.model.ImagesResponse;

public class MainFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = MainFragment.class.getSimpleName();

    private static final String SHUTTER_STOCK_IMAGES_URL = "https://api.shutterstock.com/v2/images/search";
    private static final String CLIENT_ID = "9b3a81633457ce29735c";
    private static final String CLIENT_SECRET = "6085bf5542adac636bccfb2ceefab18871652925";

    private static final String PARAM_AUTHORIZATION = "Authorization";

    private NetworkCommunicationManager communicationManager;

    private EditText searchEditText;
    private View searchButton;

    private ImagesListAdapter listAdapter;

    private ProgressDialog progressDialog;

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        communicationManager = NetworkCommunicationManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        searchEditText = (EditText) rootView.findViewById(R.id.search_edit_text);
        searchButton = rootView.findViewById(R.id.search_button);
        searchButton.setOnClickListener(this);

        ListView listView = (ListView) rootView.findViewById(R.id.list_view);
        listAdapter = new ImagesListAdapter(getActivity());
        listView.setAdapter(listAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        communicationManager.cancelAllRequests(TAG);

        super.onStop();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == searchButton.getId()) {
            String searchText = searchEditText.getText().toString();
            if (!communicationManager.isNetworkAvailable()) {
                Toast.makeText(getActivity(), R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
            } else if (searchText.isEmpty()) {
                Toast.makeText(getActivity(), R.string.error_input_search_text, Toast.LENGTH_SHORT).show();
            } else {
                getImages(searchText);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void getImages(String searchText) {
        HashMap<String, String> headers = new HashMap<String, String>();
        String basicAuth = "Basic " + Base64.encodeToString((CLIENT_ID + ":" + CLIENT_SECRET).getBytes(), Base64.NO_WRAP);
        headers.put(PARAM_AUTHORIZATION, basicAuth);

        String searchTextEncoded = null;
        try {
            searchTextEncoded = URLEncoder.encode(searchText, "utf-8");
        } catch (UnsupportedEncodingException ex) {
            Log.e(TAG, "searchTextEncoded - encoding error", ex);
        }

        String url = SHUTTER_STOCK_IMAGES_URL + "?query=" + searchTextEncoded;
        GsonRequest request = new GsonRequest(url, ImagesResponse.class, headers, new Response.Listener<ImagesResponse>() {

            @Override
            public void onResponse(ImagesResponse response) {
                Log.d(TAG, "onResponse");
                Image[] images = response.getData();
                if (images.length > 0) {
                    listAdapter.setImages(response.getData());
                } else {
                    Toast.makeText(getActivity(), "No images found for the current search!", Toast.LENGTH_SHORT).show();
                }

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse", error);

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        });
        request.setTag(TAG);

        progressDialog = ProgressDialog.show(getActivity(), "Loading", "Wait while loading...");

        communicationManager.addToRequestQueue(request);
    }

}
